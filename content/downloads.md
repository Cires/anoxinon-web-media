+++
title = "Downloads"
description = "Downloads"
keywords = ["extras","Downloads","Links","Anxicon"]


+++

Nachfolgend eine Auflistung von Downloads:

---

**Flyer**

- <a href="/files/Anleitung_Conversations.pdf">Anleitung für Conversations</a> - Von Messtome, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.de">CC-BY-SA 4.0</a>
- <a href="/files/Flyer_Linux_final_Webseite.pdf">Gründe für Linux - Website Edition</a> - Von Anoxinon & Messtome, <a href="/lizenzhinweis/">CC-BY 4.0</a>
- <a href="/files/Flyer_Datenschutz_final_Webseite.pdf">Datenschutz für Klein & Gross - Website Edition</a> - Von Anoxinon & Messtome, <a href="/lizenzhinweis/">CC-BY 4.0</a>

---

> Die Auflistung ist derzeit unvollständig

<br><br><br>
