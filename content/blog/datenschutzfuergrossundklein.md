+++
title = "Datenschutz für Klein und Groß"
date = "2019-05-03T18:20:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz", "Freie Software"]
banner = "/img/thumbnail/Datenschutz_klein-groß_klein_1.png"
description = "Sicherer Umgang mit Medien - Das ist das Motto unseres heutigen Beitrages. Wir möchten eine Übersicht für Eltern bieten, unter anderem worauf man achten sollte."
+++

>Dieser Beitrag ist für Anfänger geeignet; Letzte Aktualisierung 08.05.19 - 18:00

---

### I. Einführung

![](/img/kleingross/Carte2016.png)

<small>Picture credit: Simon « Gee » Giraudot, Creative Commons By-SA 4.0, [Thanks to Framasoft](http://framasoft.org)</small>

"Alle 18 Minuten kam im letzten Jahr ein Schulkind, im Straßenverkehr, zu Schaden.“

So beginnt die Kampagne des „[Schutzranzen](https://mobilsicher.de/aktuelles/schutzranzen-fragwuerdiges-kinder-tracking-per-app)“, einem privaten Projekt, dass verspricht, den Straßenverkehr für Kinder sicherer zu machen, indem die Positionen von Kindern in Echtzeit an Autofahrer in der Nähe und an die Eltern übermittelt werden.

Die „[Hello Barbie](https://welt.de/kultur/article139863883/Eine-Barbie-mit-WLAN-ist-das-Ende-der-Kindheit.html)“ Puppe nutzt WiFi und Spracherkennungstechnologie, um in einen interaktiven Dialog zu treten“ (frei übersetzt) – so die Produktbeschreibung eines Spielzeuges.

Ein weiteres Produkt dieser Kategorie, die "[Toniebox](https://kuketz-blog.de/toniebox-kinderfreundliches-bedienkonzept-und-der-datenschutz/)", ist ein würfelförmiges Tonabspielgerät für Kleinkinder. Das Gerät besitzt eine eindeutige Toniebox-ID und nutzt Wlan um eine Verbindung zur Toniecloud aufzubauen. Dabei wird jede Aktion des Nutzers aufgezeichnet und ausgewertet.

Alle genannten Produkte, seien sie auch nur einzelne Beispiele, versprechen Bequemlichkeit – eine Ein-Knopf-Lösung, um das Kind abzusichern oder es zu beschäftigen.
Bereits nach der Ankündigung o.g. Produkte kam es zu massiver Kritik von Verbraucherschützern, Verbänden für den Datenschutz und Organisationen für die Rechte von Kindern.

Wie wichtig Datenschutz und letztlich auch das Grundrecht auf Privatsphäre bereits im Kindesalter ist, zeigt nicht zuletzt eine [Studie des University College London](https://ucl.ac.uk/news/news-articles/0915/040915-caring-parents-happier-lives) – Kinder, die einer zu starken elterlichen Kontrolle ausgesetzt sind, haben eine schlechtere mentale Gesundheit als andere. Dies fängt nicht erst mit dem Einsatz von Überwachungsgeräten [wie einer Smartwatch für Kinder](https://vodafone.de/featured/inside-vodafone/v-kids-watch-neue-vodafone-smartwatch-verbindet-dich-mit-deinen-kindern/), die Google Maps den Standort mitteilt ein.

Da dieses Thema auch nicht vor der frühkindlichen Bildung halt macht, ist es auch eine wichtige Aufgabe der Vorschulen (Kindergärten) bzw. Grundschulen, bereits darauf aufmerksam zu machen, damit die Weichen für eine gute Zukunft des Kindes gestellt werden können. Dabei müssen selbstverständlich auch die Eltern der Kinder mit einbezogen werden – schließlich sind sie es, die entscheiden, wann der Sprössling das erste Smartphone erhält und soziale Medien und Messenger nutzen darf. Damit Eltern solche Produkte und Projekte nicht ohne weitere Gedanken nutzen, bedarf es Aufklärungsarbeit. Im Rahmen unserer Datenschutz für Klein und Groß Aktion, haben wir uns daher umfassend mit Möglichkeiten für Kindergärten und Vorschulen befasst.
Daher gibt es im Überblick kurz ein paar Antworten zu den wichtigsten Fragen. Wir nehmen hier an, dass Sie ggf. das erste Mal auf unserer Seite sind und auch noch nicht viel oder keine Erfahrung mit Open Source etc. haben (unsere Dauerleser mögen es uns nachsehen, wenn wir uns hier an manchen Stellen wiederholen).

**Was ist Datenschutz und wieso sollte man darauf achten?**

Würden Sie einem Fremden Ihre persönlichen Informationen anvertrauen, Ihre Kommunikation zur Verfügung stellen oder einfach nur überwachen lassen was sie wie, wo, wann und mit wem machen? Wenn nicht, hat sich diese Frage damit bereits beantwortet. Man sollte nach dem Motto "Vertrauen ist gut, Kontrolle ist besser" handeln.  Auch haben wir bereits Artikel veröffentlicht "[Wieso brauchen wir Datenschutz?](https://anoxinon.media/blog/wiesobrauchenwirdatenschutz/)". Sie sagen Ihrem Kind wahrscheinlich, dass es nicht mit Fremden reden soll. Wenn ihr Kind das tun soll, dann gehen Sie mit gutem Beispiel voran und geben Sie die Informationen Ihres Kindes auch nicht an Fremde.

**Ihr Kind hat (digitale) Rechte**

Dazu gehört auch das Recht am eigenen Bild. Sie können nicht sicher sein, dass alles, was Sie jetzt gerade verschicken, fotografieren und fleißig teilen irgendwann öffentlich wird und Ihrem Kind schadet (z. B. bei der Berufswahl oder seelisch). Kinder müssen sich frei entwickeln. Auch, wenn Sie die Fotos Ihres Kindes nur "lokal" auf dem Smartphone speichern. Haben Sie dort eine Firewall? Und nein, eine Firewall und ein Anti-Virenprogramm sind nicht dasselbe. Anti-Virenprogramme auf Ihrem Smartphone schaden meist mehr, als das diese einen Nutzen hätten. Vertrauen Sie darauf, dass Google, Apple oder Microsoft wirklich denn Zugriff auf die Fotos Ihrer Apps unterbinden? Das diese Fotos nicht in die Cloud synchronisiert werden? Ihr Smartphone verbindet sich ständig zu den Servern des Anbieters. Eine sehr ausführliche Anleitung wie man sein Smartphone schützen kann, gibt es auf dem [Kuketz Blog](https://kuketz-blog.de/your-phone-your-data-teil1). Vorsicht aber, diese Anleitung benötigt etwas Technikwissen.

---

### II.Was kann ich tun?

**Was ist Open Source?**

So wie im Kindergarten Gruppen eine Gemeinschaft bilden, gibt es bei Softwareentwicklern Gemeinschaften, die sich zusammentun, um nicht von den großen Softwareriesen abhängig zu sein. Das erschaffene Gut, wird dann meist kostenlos und am Programmiertext einsehbar veröffentlicht. Jeder darf es nutzen, erweitern und sicherstellen, dass das Programm auch nur das tut, was es soll. Wir sprechen hier auch von "offener" Software. Das Pendant dazu ist Closed Source, also Software bei denen die Nutzung einer Blackbox gleicht, keiner weiß wohin die Daten fließen. Das sind z. B. Windows und Apple Produkte. Ein sehr gutes Beispiel für Open Source Projekte sind die Linux Distributionen, die als sehr sichere Betriebssysteme gelten. Mehr Informationen finden Sie auch bei unserem Beitrag „[Ist Open Source seriös?](https://anoxinon.media/blog/istfreiesoftwareserioes/)“.  Auch zu [Linux](https://anoxinon.media/blog/linuxfuereinsteiger1/) als Betriebssystem haben wir bereits einige [Artikel](https://anoxinon.media/blog/linuxfuereinsteiger1/) und Gründe verfasst.


**Was ist so schlimm daran, wenn ich Bilder von meinem Kind via Messenger (Whatsapp und Co. versende)? <br>Die sind doch verschlüsselt!**


Stellen Sie sich ein Haus vor, Sie haben einen Schlüssel einer unbekannten Person von der Sie wenig wissen...ist Ihr Haus dann sicher, zumal der Fremde auch noch die Baupläne für Ihr Haus hat? Diese Messenger sind zudem nicht Open Source und leben meist vom Verkauf der Daten. Sie wissen nicht, wo die Daten Ihres Kindes landen werden. Oft ist eine sogenannte Hintertüre in den Messengern eingebaut, die den Zugriff auf den Nachrichteninhalt ermöglicht. Das meiste Geld wird mit den sogenannten "Verkehrsdaten" verdient. (z. B. Wer, Wo, Wie, Wann). Threema wird gerne als Alternative genannt. Es ist zwar ein Messenger aus der Schweiz, jedoch baut diese ihre Überwachungsbefugnisse derzeit aus. Das von Threema genutzte Verschlüsselungsprotokoll NaCl ist zwar für jeden einsehbar, doch die Implementierung von Client und Server bleiben bei Threema ebenfalls geheim. Threema ist also keine richtige Alternative, da ähnliche Kritikpunkte wie bei Whatsapp existieren. Eine wirklich offene und sichere Alternative ist zum Beispiel das XMPP Netzwerk. Auf Anoxinon Media finden Sie eine Erklärung sowie eine [Anleitung](https://anoxinon.media/files/Anleitung_Conversations.pdf) zu XMPP inkl. passenden Clients und wenn Sie dies nicht selbst hosten möchten: Wir haben hier eine [eigene Instanz](https://anoxinon.de/dienste/anoxinonmessenger/).


**Nochmal kurz für die, die nicht nochmal im Text suchen wollen:**

* XMPP Anmeldung & Client Einrichtung als Video (folgt demnächst)
* [Conversations Anleitung](https://anoxinon.media/files/Anleitung_Conversations.pdf)
* [Link zu unserer Instanz](https://anoxinon.de/dienste/anoxinonmessenger/) und [Link zu anderen Anbietern](https://datenschutzhelden.org/serverliste/) (falls man den dezentralen Gedanken lebt)

**App Minimalismus**


Wer Millionen Apps installiert hat, sorgt auch für Millionen potenzieller Sicherheitslücken auf seinem Handy. Man sollte sich genau überlegen welche Apps man wirklich braucht und die unnützen deinstallieren.  Vor der Installation einer neuen App ist die Vertrauenswürdigkeit zu überprüfen. Gerne auch einen Blick in unseren [Artikel Digitaler Frühjahresputz ](https://anoxinon.media/blog/digitaler_fruehjahresputz/)werfen.

**Potenzielle Gefahren**


Überall wo Technik im Einsatz ist, gibt es auch Risiken. Sprachassistenten (Alexa, Siri, Cortana, OK Google usw.) übermitteln meist ungefiltert Ihre Sprachbefehle an den Anbieter, zur "Qualitätsverbesserung". Ob die Daten am Ende Ihrem Persönlichkeitsprofil zugeordnet werden, ist undurchsichtig. Auch können solche Daten jederzeit gehackt werden. Wollen Sie Ihre privaten Gespräche innerhalb Ihrer 4 Wände wirklich jedem zugänglich machen? Wie Sie bereits wissen, gibt es heutzutage viele technische Risiken, zuletzt bei vernetztem Kinderspielzeug, welches vom Markt genommen wurde. Das Problem ist, dass noch genug schlecht gesicherte und vernetzte Geräte in Ihrem Haushalt (Laptop, Smartphones, Smart TV, Smart Watch usw.) sind an die noch nicht einmal gedacht wurde, weil es "alle" nutzen.  Das Problem einer Ansammlung von Daten ist immer das Missbrauchspotenzial.


Wären Ihre Sprachassistenten wirklich „**_smart_**“, dann würden Sie für die einfachsten Dinge wie „Licht einschalten“ keine Internetverbindung benötigen. Denken Sie daran, auch ihr Smartphone hat meist automatisch einen Sprachassistenten aktiv, diesen sollten Sie unbedingt deaktivieren sowie den meisten Apps den Zugang zu Ihrem Mikrofon unterbinden.



Eine mögliche Alternative wäre z. B. „[Snips](http://snips.ai)“, ein Sprachassistent zum einfach selbermachen, welcher ohne Cloudanbindung und komplett offline funktioniert. Das ist zu kompliziert und zu unbequem? Selber machen? Diese Alternative ist u. a. Für einen [RaspberryPi](http://raspberrypi.org) verfügbar, einem kleinen 1-Platinen-Computer, der für Schüler entwickelt wurde, um Ihnen Technik näher zu bringen. Die Installation ist also kinderleicht und auch für Anfänger mit etwas einlesen kein Problem. Zudem können Sie mit einem Eltern-Kind-Projekt super das Technikinteresse Ihres Kindes fördern und Sie verbringen Zeit zusammen mit einem interessanten Projekt anstatt vor der Glotze oder dem Smartphone ;-)


Schützen Sie Ihre Kinder und informieren Sie sich! Es gibt zu allem eine Alternative auch wenn diese etwas unbequemer erscheint.

---

### III. Fragerunde

**Wie sieht es mit Planungen von z. B. Kindergartenfeiern oder teilen von Bildern über amerikanische Cloudanbieter aus?**

Ausländische Anbieter haben keine Datenschutzsstandards, wie wir sie in Deutschland oder Europa kennen. Auch ein Standort in Europa schützt nicht unbedingt vor ausländischen Zugriffen (Cloud Act etc.). Vermeiden Sie es also Dropbox, OneDrive und Co. zu benutzen. Wenn es unbedingt eine Cloudlösung sein muss, setzen Sie auf selbstgehostete Lösungen wie z. B. Nextcloud. Alternativ scheint auch eine Lösung von [Mailbox](mailbox.org) als vertretbar.

Auch die Fritzbox hat Möglichkeiten, dass hier eine eigene Cloud eingerichtet werden kann. Es ist besser, als amerikanische Anbieter – allerdings bedenken Sie, dass der Anbieter dann jederzeit vollen Zugriff auf Ihre Daten hat! Sollten Sie dies dennoch wünschen, empfehlen wir Ihnen die dort liegenden Daten zu verschlüsseln. Wie das geht erfahren Sie [hier](https://cryptomator.org/de/).

**Wie bringe ich meinem Kind so ein komplexes Thema nahe?**

Wir wissen, dass ein Kind, was heute geboren wird, Datenschutz ggf. nicht mehr kennen wird, wenn Sie nichts unternehmen. Es wird vielleicht nie das Privileg haben, ein privates Gespräch zu führen oder sich unbeobachtet zu fühlen. Aber wie bringe ich so ein Thema meinem Kind nahe?

Es ist natürlich wichtig, dass Sie ein gutes Vorbild in dieser Sache sind. Nichts ist scheinheiliger, als selbst Instagram Profile zu haben und sein Leben auf Youtube zu teilen und seinem Kind dann zu sagen, wie wichtig Datenschutz ist. Also seien Sie ein gutes Vorbild, informieren Sie sich regelmäßig und das am besten nicht auf den Mainstreamseiten ;-)

Außer natürlich auf unserer Seite können Sie auch hier passende Informationen zum Thema Datenschutz finden:

* [digitalcourage.de](https://digitalcourage.de)
* [privacy-handbuch.de](https://privacy-handbuch.de)
* [kuketz-blog.de](https:/kuketz-blog.de)
* [netzpolitik.org](https://netzpolitik.org)
* [framasoft.org](https://framasoft.org) Englisch/Französisch
* [systemli.org](https://systemli.org)


Erzählen Sie Ihrem Kind, warum Sie diese App nicht installieren. Fragen Sie Ihr Kind einmal:

**"[Name des Kindes], würdest du einem gesichtslosen Fremden auf der Straße, der dir Schokolade/dein Lieblingsspielzeug gibt, alle Informationen und Geheimnisse deiner Freunde sagen?"**

Ziemlich sicher, dass Ihr Kind "nein" sagt. All das ist natürlich nur eine Metapher. Der gesichtslose Fremde ist ein Internetunternehmen, Schokolade/Lieblingsspielzeug die Sucht (bei vielen WhatsApp) und Informationen/Geheimnisse ist gleichgeblieben.
Kinder sind *smarter*, als Sie vielleicht denken und im Gegensatz zu Geräten, die Sie für „smart“ halten, funktionieren Sie richtig smart – nämlich offline ;-)


Kleiner Scherz am Rande, Sie wissen worauf wir hinaus wollen. Wenn Sie hier die Bildung Ihres Kindes noch etwas weiter fördern wollen: Marc Uwe-Kling (Autor der Känguru Chroniken) hat ein sehr schönes Kinderbuch zu diesem Thema herausgebracht: „[Der Tag an dem Oma das Internet kaputt gemacht hat](https://marcuwekling.reimkultur-shop.de/marc-uwe-kling-der-tag-an-dem-die-oma-das-internet-kaputt-gemacht-hat-kinderbuch-6400391.html)“. Wir möchten kurz erwähnen, dass wir für diese Nennung weder einen Deal mit Marc-Uwe Kling haben noch in irgendeinerweise gesponsored wurden.


Und bevor Sie sich das in Ihren Amazon Einkaufswagen legen, gehen Sie lieber zu Ihrer lokalen Buchhandlung um die Ecke, sehen sich einmal die Website von [Buch7](https://buch7.de) an, beziehen es von der                                   

[Digitalcourage](https://digitalcourage.de/blog/2018/rezension-der-tag-an-dem-die-oma-das-internet-kaputt-gemacht-hat) oder gleich bei [Herrn Kling](https://marcuwekling.de).
Mit letzteren tun Sie nicht nur Ihrem Kind etwas gutes, sondern lt. Herrn Klings Website:

>Das Recht auf Privatsphäre und das Recht auf Informationsfreiheit sind Teil der unveräußerlichen Menschenrechte. Doch gerade durch das Internet haben sich die Lebensumstände für eine Mehrheit der Menschen komplett verändert und leider nicht zum Besseren. Unter falschen Vorwand werden Daten gesammelt, Telefonanrufe und E-Mails gespeichert und mit sogenannten „Treueprogrammen“ Einkäufe und Konsumverhalten aufgezeichnet. Die p≡p Stiftung befürwortet den Datenschutz und hat für diesem Zweck p≡p engine entwickelt, um so die digitale Kommunikation sicher zu machen und mit nur einem Klick zu verschlüsseln. p≡p engine wird als Freie Software zur Unterstützung der Privatsphäre für alle verteilt.

Ebenfalls wurde von der Digitalcourage das Lexikon "[Kids digital genial](https://digitalcourage.de/sites/default/files/2018-06/Auszug_KidsDigitalGenial_Leseprobe.pdf) (Leseprobe)" herausgegeben mit Tipps zum[ Einsatz im Unterricht](https://kidsdigitalgenial.de/unterricht). Das Lexikon kann im[ Webshop der Digitalcourage](https://shop.digitalcourage.de/kategorie/buecherbroschueren/kids-digital-genial.html) erworben werden.

---

###  IV . Wie erreiche ich nun mehr Datenschutz?

![](/img/kleingross/GAFAM.png)



<small>Picture credit: Simon « Gee » Giraudot, Creative Commons By-SA 4.0, [Thanks to Framasoft](https://framasoft.org)</small>


In unserem Flyer haben wir bereits die Punkte angesprochen (Nutzung von Alternativen, Internetbrowsern, E-Mail Anbietern mit guten Datenschutzniveau). Doch jeder E-Mail Anbieter verspricht viel Datenschutz, Sie als Laie werden hier wenig Überblick haben und am Schluss wieder auf Werbeversprechen hineinfallen. Deswegen geben wir ein paar Empfehlungen.

**E-Mail Anbieter mit guten Datenschutz Niveau:**

Von Unternehmen:

* [mailbox.org](https://mailbox.org)

E-Mail Anbieter, die mit "[Email made in Germany](https://media.ccc.de/v/30C3_-_5210_-_de_-_saal_g_-_201312282030_-_bullshit_made_in_germany_-_linus_neumann)" (Gmx, Web.de, Freemail etc. ) werben, zählen für uns nicht zu den Pionieren in Sachen Datenschutz.

**Von privaten Anbietern**

* [systemli.org](https:/systemli.org)
* [dismail.de](https://dismail.de)

**XMPP als Whatsapp Alternative**

Wie bereits oben erwähnt, eignet sich XMPP hervorragend als Alternative.
Registrieren kann man sich z. B. bei:

* [anoxinon.de](https://anoxinon.de)
* [trashserver.net](https://trashserver.net)
* [wiuwiu.de](https://wiuwiu.de)
* [tchncs.de](https://tchncs.de)
* [systemli.org](https://systemli.org)

**Welche Suchmaschine achtet meine Privatsphäre?**

* [startpage.com](https://startpage.com)
* [searx.me](https://searx.me)
* [metager.de](https://metager.de)

Oft werden noch DuckDuckGo und Ecosia genannt, welche wir jedoch nicht in das Ranking aufnehmen. DuckDuckGo hat seinen Standort in den USA und Ecosia wirbt zwar mit Spenden für die Umwelt nutzt aber viele Facebook Tracker. Daher ist klar, durch wen diese Spenden möglich sind. Auch Posteo hat es auf Grund der jüngsten Kritik leider nicht ins Ranking geschafft.

**Terminvereinbarung**

Doodle hat sich innerhalb von kürzester Zeit zum "Marktführer" hier entwickelt, da aber auch hier Suchmaschinen und Werbeunternehmen Ihre Daten bekommen, raten wir auch hier zu einer datenschutzfreundlichen Methode. Bitte beachten Sie **NIE** Ihren vollen Namen einzutragen. Sie wissen nicht, welche Bots etc. darauf zugreifen.

* [Croodle](https://systemli.org/en/service/croodle.html)
* [Dudle](https://dudle.inf.tu-dresden.de/)

**Welcher Internetbrowser nimmt Datenschutz noch ernst?**

Firefox steht momentan ziemlich alleine da, jedoch muss auch hier deutlich angepasst werden und zumindest [ublock](https://addons.mozilla.org/de/firefox/addon/ublock-origin/reviews/?lang=en-us) installiert werden. Ein sehr guter Artikel, wie man höchstmögliche Datensicherheit unter Firefox erreicht, was welche Addons bewirken und wie man diese steuert finden Sie [hier](https://kuketz-blog.de/firefox-ein-browser-fuer-datenschutzbewusste-firefox-kompendium-teil1/).

**Alternative Cloudlösungen**

* [Nextcloud](https://nextcloud.org)
* [mailbox.org](https://mailbox.org)
* Fritzbox (dort Einstellungen setzen)

**Wie befreie ich mein Smartphone und was ist eigentlich Bloatware?**

Eine sehr gute und ausführliche Anleitung liefert hier der [Kuketz Blog](https://kuketz-blog.de/your-phone-your-data-teil1/).

**Linux Installation**

Unter [gute Gründe für Linux](https://anoxinon.media/blog/linuxfuereinsteiger1/) und in unserem Artikel über [Linux Distributionen und Installationshilfen](https://anoxinon.media/blog/linuxfuereinsteiger2/), zeigen wir Ihnen wie Sie Linux installieren können und was die genauen Vorteile sind (neben mehr Datenschutz!)
Wir hoffen natürlich Sie etwas Neugierig auf das Thema gemacht zu haben. Selbstverständlich sind dies nur wenige Tipps von vielen, die wir anbieten. Wir möchten ein möglichst großes Spektrum an Menschen erreichen und hoffen, dass Sie sich auch die Tipps auf den weiteren Seiten zu Herzen nehmen.
Wer mehr zu Alternativen erfahren möchte, kann sich gerne unsere kleine Liste mit Alternativen ansehen.

---

###  V. Schlussworte

![](/img/kleingross/Peha-Banquet-Degooglisons-CC-By.png)

<small>Picture credit: Simon « Gee » Giraudot, Creative Commons By-SA 4.0, [Thanks to Framasoft](http://framasoft.org)</small>


Also bedenken Sie, Sie gestalten die Zukunft Ihres Kindes mit. Sie sind für die digitale Bildung
Ihres Nachwuchses verantwortlich. Nicht die Lehrer, nicht die Anderen, sondern Sie!
Was wollen Sie Ihrem Kind sagen, wenn es mal auf Sie zukommt mit der Frage: “Warum hast du
nichts dagegen gemacht?  Warum hast du mich dem ausgesetzt?”
“Ich wusste es, aber es war mir egal. Wir konnten nichts tun, es haben ja alle so gemacht auch wenn
es nicht richtig war”  ODER “Als die ganze Welt von Datenkraken besetzt war, war ich teil einer
Gemeinschaft von Unbeugsamen, die nicht aufgehört haben den Eindringlingen Widerstand zu
leisten!”

Unseren kostenlosen Infoflyer, für Eltern, finden Sie hier:<br><br>

<div style="text-align: center">  <a href="/files/Flyer_Datenschutz_final_Webseite.pdf" class="btn btn-small btn-template-main">Download Flyer</a></div>
<br><br>
Nicht nur unsere Flyer sind sehenswert, sondern auch der des
[Förderverein ProWOZ: Anleitung zur digitalen Selbstverteidigung](https://konsumentenschutz.ch/sks/content/uploads/2018/10/digi-ratgeber_okt18.pdf).
