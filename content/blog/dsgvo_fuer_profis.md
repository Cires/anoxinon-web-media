+++
title = "DSGVO für Profis"
date = "2019-03-15T16:45:00+02:00"
tags = ["Profis"]
categories = ["Allgemeines", "Datenschutz"]
banner = "/img/thumbnail/Flagge_DSGVO.png"
description = "Die DSGVO war viel in den Medien aber was genau bedeutet das für ein Unternehmen oder eine Organisation?"

+++
> Dieser Inhalt steht unter einer freien Lizenz, weitere Informationen sind <a href="/lizenzhinweis/">hier</a> zu finden.

Liebe Leser,

diesmal gibt es keinen "normalen" Artikel. Wir haben uns dafür entschieden DSGVO Schulungsmaterial (ohne Gewähr) zur Verfügung zu stellen. Wir haben einiges an Mühe in diesen Beitrag gesteckt und richten uns hiermit nicht an den Otto-Normal-Verbraucher.

Wir erklären, wann ein Datenschutzbeauftragter beauftragt werden muss, welche Fragen sich Mitarbeiter/Abteilungen selbst stellen müssen, wie man am besten mit der Arbeit beginnt und wie eine Risikoanalyse erstellt wird.

Auf Grund der Fülle an Informationen, empfanden wir es für den Leser als angenehmer, die Informationen als Präsentation bereitzustellen.

**Hinweis:** Wir betreiben keine Rechtsberatung sondern versuchen einen Überblick zu verschaffen.

Wir würden uns über Feedback/Kritik/Anregungen freuen. :)

<br><br>

<div style="text-align: center">  <a href="/files/DSGVO_PROFIS_HANDOUT.pdf" class="btn btn-small btn-template-main">Download Handout</a>  |   <a href="/files/DSGVO_PROFIS.odp" class="btn btn-small btn-template-main">Download Präsentation</a></div>
