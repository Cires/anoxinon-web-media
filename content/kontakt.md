+++
title = "Kontakt"
id = "contact"
+++

<h3>Fragen, Anregungen, Mitmachen?</h3>

Wir freuen uns über jegliche Kontaktaufnahme, seien es Fragen, Verbesserungsvorschläge oder allgemeine Anfragen.
Solltest Du Interesse haben mitzuwirken, schaue hier vorbei: <a href="https://anoxinon.de/mitmachen/">Mitmachen</a><br>
Unsere Kommunikationskanäle haben wir weiter unten aufgelistet.
<br><br>
<b>Kontakt via E-Mail:</b>

Redaktion - Leitung: <a href="mailto:redaktion@anoxinon.de">redaktion@anoxinon.de</a><br>
GPG Key: <a href="/keys/0x4038E4839A17D48D.txt">0x4038E4839A17D48D</a>
 | ID: E790 4472 54CE 0357 2C7B 2CB7 4038 E483 9A17 D48D  <br>
<br>
<b>Kontakt via Fediverse:</b>

Mastodon: <a href="https://social.anoxinon.de/@anoxinonmedia">@AnoxinonMedia@social.anoxinon.de</a><br><br>
<b>Kontakt via XMPP:</b>

XMPP MUC: <a href="xmpp://anoxinon@conference.anoxinon.me">anoxinon@conference.anoxinon.me</a>
<br><br>

<h4>Achtung: Anfragen zu dem Verein Anoxinon e.V. oder dessen anderen Dienste sind bitte <a href="https://anoxinon.de/kontakt/">direkt</a> an diesen zu richten.</h4>
