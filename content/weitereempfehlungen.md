+++
title = "Weitere Empfehlungen"
description = "Weitere Empfehlungen"
keywords = ["extras","Downloads","Links","Anxicon","Empfehlungen"]


+++
Nachfolgend eine Auflistung von Büchern/Studien etc.

---

### Bücher ###

- "Die Daten, die ich rief" von Katharina Nocun  
- "Kultur der Digitalität" von Felix Stadler  
- "Wie künstliche Intelligenz uns berechnet, steuert und unser Leben verändert" von Ki Schlietern  
- "Qualityland" von Marc-Uwe Kling  
- "Eine kurze Geschichte der Digitalisierung" von Martin Burkhard  
- "Zehn Gründe, warum du deine Social Media Accounts sofort löschen musst" von Jaron Lanier  
- "Jäger, Hirten, Kritiker" von Richard David Precht  
- "Die Kunst der Täuschung" von Kevin D. Mitnick  
- "Der Tag, an dem Oma das Internet kaputt gemacht hat" von Marc Uwe-Kling

---

### Flyer / Broschüren ###

- [Kids - Digital genial - Digitalcourage](https://digitalcourage.de/sites/default/files/2018-06/Auszug_KidsDigitalGenial_Leseprobe.pdf) Lexikon der Digitalcourage (Leseprobe)  
- [EDRI Digital Defenders](https://edri.org/files/privacy4kids_booklet_web.pdf) sehr gute Informationsbroschüre für Kids (erhältlich in Englisch, Deutsch und Französisch)  

---

### Berichte / Studien ###

- [Windows 10 Telemetrie (Bund)](https://bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/SiSyPHus/Analyse_Telemetriekomponente.pdf?__blob=publicationFile&v=4) Analyse aller Telemetriekomponenten in Windows 10  
- [Deceived by Design](https://fil.forbrukerradet.no/wp-content/uploads/2018/06/2018-06-27-deceived-by-design-final.pdf) Studie darüber, wie sich die großen Hersteller immer perfidere Methoden überlegen, dass Menschen sich gegen Datenschutz entscheiden in dem sie datenschutzunfreundlichere Maßnahmen einfacher verfügbar machen  
- [Google Data Collection Paper](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf) Studie darüber, in welchem Umfang Google Daten sammelt  

---

### TV ###

- [Prism is a dancer](https://zdf.de/show/lass-dich-ueberwachen/lass-dich-ueberwachen-die-prism-is-a-dancer-show-vom-2-november-2018-100.html)
- [Im Rausch der Daten](https://ardmediathek.de/tv/Filme-im-Ersten/Dokumentarfilm-im-Ersten-Im-Rausch-der-/Das-Erste/Video?bcastId=1933898&documentId=52620920)  
- [Leben nach Microsoft (Achtung Youtube Video)](https://youtube.com/watch?v=V3rGn-PIX_s)  
- [Reportage über F-droid](https://zdf.de/nachrichten/heute/zehn-freundliche-android-apps-100.html)  

---

### Hörspiel & Radio ###

- [Tom Schimmeck - Silicon Dreams](https://wdr.de/radio/wdr3/programm/sendungen/wdr3-hoerspiel/download-tom-schimmeck-silicon-dreams-100.html) Hörspiel  
- [Herrschaft der Algorithmen](https://swr.de/swr2/programm/sendungen/wissen/herrschaft-der-algorithmen/-/id=660374/did=21528068/nid=660374/1orqyjs/index.html) SWR Radio  

---

Sollten wir ein Buch/eine Reportage etc. vergessen haben, die sich mit Open Source und Privatsphäre beschäftigt und das auch entsprechend umsetzt, dann schreiben Sie uns einfach eine E-Mail und wir ergänzen diese Liste gegebenfalls.
